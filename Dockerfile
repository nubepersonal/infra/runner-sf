FROM alpine:3.13

ENV ISTIOCTL_VERSION=1.10.5 \
    KUBE_VERSION=v1.17.3 \
    HELM_VERSION=v3.6.0

RUN apk update && apk upgrade && apk add --no-cache curl bash gettext tar && \
        rm -rf /var/lib/apt/lists/* && \
        rm /var/cache/apk/*

# Install Istioctl
RUN \       
        #  Version 1.10
        curl -sL "https://github.com/istio/istio/releases/download/1.10.5/istioctl-1.10.5-linux-amd64.tar.gz" | tar xvz && \
        chmod u+x istioctl && \
        mv istioctl /usr/local/bin/istioctl-1.10.5 && \
        # Add Istio User
        adduser -D istio && \
        chown istio:istio /usr/local/bin/istioctl*
        
# Install Kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/${KUBE_VERSION}/bin/linux/amd64/kubectl && \
        chmod u+x kubectl && \
        mv kubectl /usr/local/bin/kubectl && \
        chown istio:istio /usr/local/bin/kubectl

# Install Helm
RUN curl -sL https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz | tar xvz && \
        chmod u+x linux-amd64/helm && \
        mv linux-amd64/helm /usr/local/bin/helm && \
        chown istio:istio /usr/local/bin/helm

USER istio